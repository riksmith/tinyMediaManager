package org.tinymediamanager.scraper.tvmaze.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImageUrls extends BaseJsonEntity {
  public String medium;
  public String original;
}
