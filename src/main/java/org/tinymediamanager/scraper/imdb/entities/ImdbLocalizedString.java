package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbLocalizedString extends BaseJsonEntity {
  public String value    = "";
  public String language = "";
}
