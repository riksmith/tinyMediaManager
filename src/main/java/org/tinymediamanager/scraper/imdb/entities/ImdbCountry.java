package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbCountry extends BaseJsonEntity {
  public String id   = "";
  public String text = "";
}
