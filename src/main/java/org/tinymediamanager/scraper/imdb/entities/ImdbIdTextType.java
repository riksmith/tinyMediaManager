package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbIdTextType extends BaseJsonEntity {
  public String id   = "";
  public String text = "";
}
